# Auto network backups and snapshots

These are a bunch of scripts made to sync my desktop / laptop files to my home server. There's another script included that makes --reflink copies of them, separating by date. This is made to run on a Linux OS like Arch Linux.

# Dependencies

You should make sure that these dependencies are met:

Client: bash, netcat, rsync and any on-demand network filesystem like sshfs, ftp, nfs or maybe smb (smb is slow, I don't recommend) mounted via x-systemd.automount or autofs.

Server: A Copy-On-Write capable filesystem, like BTRFS.

# How I'm currently transfering files between server and client

I have a few protocols for transfering files between them:

- [x] SMB / CIFS
- [x] NFS
- [x] SFTP
- [x] SSHFS
- [x] FTP

And I also have some requirements:

- [x] The transfer speeds must be fast enough to saturate my network speed (currently 1Gbps)
- [x] Shared folders will only be avaliable with good authentication
- [x] I want to access it as a local filesystem in order to edit videos and read .iso files directly from the server
- [x] The server folder should be mounted automagically when I open a software, which wants to load some files inside it
- [x] I don't want to use something painful to set up

Analyzing each option:
- [x] SMB/CIFS is slow as hell when uploading files to the server with mount.cifs
- [x] NFS is fast, but has a very poor authentication method by default. In order to improve security, there's a bunch of ways, which are all very painful to setup
- [x] FTP was kinda confusing to mount as a filesystem and it also was [dumping core](https://bugs.archlinux.org/task/69758) on server-side
- [x] SFTP works fine, but cannot be mounted as a filesystem
- [x] SSHFS is basically SFTP but mounted as a filesystem. yay!

Since SSHFS was the best option overall, I have to know how to make it work. 

First of all, I have to install the package sshfs and authenticate ssh with public/private key, but without password protection. After that, I need to specify where I want to mount and give ownership of it to my user:

    mkdir /myghi-server
    chown myghi63:myghi63 /myghi-server

With that done, the most modern way to mount this directory on demand is by using x-systemd.automount on the fstab entry:

    myghi63@myghi-server:/folder/to/mount       /myghi-server     fuse.sshfs      noauto,uid=myghi63,gid=myghi63,port=SSHPORTHERE,IdentityFile=/home/myghi63/.ssh/id_ed25519,x-systemd.automount,x-systemd.idle-timeout=60,x-systemd.mount-timeout=15,_netdev,reconnect,ServerAliveInterval=15,allow_other,users   0 0

Then, edit the file /etc/fuse.conf and remove # from user_allow_other and save.

Now mount the folder as root once, to save your server details to root's known_hosts. Do this, otherwise the auto-mounting on demand will fail

    sudo mount /myghi-server

With all that done, rebooting the computer is the easiest way to make sure systemd will auto-mount it. To check if it's working, navigate to the mount point on the file explorer and check if it mounted automagically. If doesn't, try to mount on the terminal and fix it, based on the error it gives to you.
